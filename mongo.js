db.fruits.insertMany(
    [
        {
          name: "Apple",
          supplier: "Red Farms Inc.",
          stocks: 20,
          price: 40,
          onSale: true
        },

        {
          name: "Banana",
          supplier: "Yellow Farms",
          stocks: 15,
          price: 20,
          onSale: true
        },

        {
          name: "Kiwi",
          supplier: "Green Farming and Canning",
          stocks: 25,
          price: 50,
          onSale: true
        },
        {
          name: "Mango",
          supplier: "Yellow Farms",
          stocks: 10,
          price: 60,
          onSale: true
        },
        {
          name: "Dragon Fruit",
          supplier: "Red Farms Inc.",
          stocks: 10,
          price: 60,
          onSale: true
        }
    ]

)

db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}}
])

db.fruits.aggregate([
	{$match:{onSale:true}},
	{$group: {_id:"$supplier",totalStocks:{$sum:"stocks"}}}

	])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:null,totalStocks:{$sum:"$stocks"}}}
	])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"AllOnSaleFruits",totalStocks:{$sum:"$stocks"}}}

	])

db.fruits.aggregate([

	{$match: {supplier:"Red Farms Inc."}},
	{$group: {_id:"RedFarmsInc",totalStocks:{$sum:"$stocks"}}}

	])
    
    db.fruits.aggregate([

	{$match: {$and:[{supplier:"Yellow Farms"},{onSale:true}]}},
	{$group: {_id:"YellowFarms",totalStocks:{$sum:"$stocks"}}}

	])

db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id:"$supplier", avgStock: {$avg:"$stocks"}}}
	])

db.fruits.aggregate([
        {$match: {onSale:true}},
        {$group: {_id:null,avgPrice:{$avg:"$price"}}}
    ])

db.fruits.aggregate([
	
	{$match:{onSale:true}},
	{$group:{_id:"highestStockOnSale", maxStock: {$max:"stocks"}}}
	])
db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:null,maxPrice: {$max: "$price"}}}	
])
db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group: {_id:"lowestStockOnSale", minStock:{$min:"$stocks"}}}
])

db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group: {_id:"lowestPriceOnSale", minPrice:{$min:"$price"}}}
])

db.fruits.aggregate([
	{$match: {price:{$lt:50}}},
	{$group: {_id:"minStockLess50", minStock:{$min:"$stocks"}}}
])

db.fruits.aggregate([
	{$match: {price:{$lt:50}}},
	{$group: {_id:"minStockLess50", minStock:{$min:"$stocks"}}}
])
db.fruits.aggregate([
		{$match: {onSale:true}},
		{$count: "itemsOnSale"}
	])

db.fruits.aggregate([
		{$count: "allFruits"}
	])

db.fruits.aggregate([
	{$match: {price:{$lt:50}}},
	{$count: "itemPriceLessThan50"}
	]

db.fruits.aggregate([
	{$match: {stocks:{$lt:20}}},
	{$count: "forRestock"}
	])
db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}},
	{$out: "stocksPerSupplier"}
db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group: {_id:"$supplier",avgPrice:{$avg:"$price"}}},
	{$out: "stocksPerSupplier"}
	])
