
db.fruits.aggregate([
  {$match: {$and:[{supplier:"Yellow Farms"},{price:{$lt:50}}]}},
  {$count: "itemsInYellowFarmsPriceLessThan50"}
  ])

db.fruits.aggregate([
  {$match: {price:{$lt:30}}},
  {$count: "itemsPriceLessThan30"}
  ])

db.fruits.aggregate([
        {$match: {supplier:"Yellow Farms"}},
        {$group: {_id:"ItemsAvgPriceInYellowFarm", itemsAvgPrice:{$avg:"$price"}}}
    ])

db.fruits.aggregate([

  {$match: {supplier:"Red Farms Inc."}},
  {$group: {_id:"ItemsHighestPriceInRedFarmsInc",maxPrice: {$max: "$price"}}} 
])

db.fruits.aggregate([

  {$match: {supplier:"Red Farms Inc."}},
  {$group: {_id:"ItemsLowestPriceInRedFarmsInc",maxPrice: {$min: "$price"}}} 
])
